
export interface NewsArticle {
  id: number;
  title: string;
  content: string;
  dateString: string;
  baseImageName: string;
  articleType: ArticleType;
  isFavourite: boolean;
}

export enum ArticleType {
  TopStory = 'TOP_STORY',
  CodeExample = 'CODE_EXAMPLE'
}

// Store root state
export interface RootState {
  topToolbar: TopToolbarState;
}

// // Store modules state
// export interface TopToolbarState {
//   title: string;
// }
//
// // Our API of the news
//
// export interface Source {
//   id: string;
//   name: string;
// }
//
// export interface Article {
//   source: Source;
//   author: string;
//   title: string;
//   description: string;
//   url: string;
//   urlToImage: string;
//   publishedAt: Date;
//   content: string;
// }
//
// export interface RootObject {
//   status: string;
//   totalResults: number;
//   articles: Article[];
// }
