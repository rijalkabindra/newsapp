
import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import {RootState} from '@/types';
import {RouterOptions} from 'vue-router';


import NewsList from '@/services/newslist';

import { topToolbar } from './modules/topToolbar';

Vue.use(Vuex);


const store: StoreOptions<RootObject> = {
  modules: {
    topToolbar
  },

  actions: {
    // NewsList.
  },

  mutations: {

  },
};

export default new Vuex.Store<RootState>(store);
