import { http } from '@/httpCommon';
import { RootObject } from '@/types'

class Newslist {
  getAll(): Promise<RootObject> {
    return http.get('')
  }
}

export default new Newslist();
